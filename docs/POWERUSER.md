# A power user's guide to Moment

This page describes the intended way to navigate Moment to reach its full
potential.
It is intended for users who participate in hundrends of chat rooms and are
active in many conversations at once.

## Jumping to received messages

If you receive a lot of messages that you would like to respond to, you may
find it tedious to keep clicking on the room list. A quicker way is to press
<kbd>Ctrl+U</kbd> to jump to the latest unread room.

## Jumping to recently visited chats

When you are active in many rooms at once, it can be useful to switch between
them quickly without needing to think which is which. For this purpose, Moment
remembers recently visited rooms, and you can quickly navigate through the
history using <kbd>Ctrl+H</kbd> and <kbd>Ctrl+L</kbd>. If you are struggling to
keep up with many ongoing conversations, we recommend utilizing this history as
much as possible.

## Jumping to specific rooms

You can use <kbd>Ctrl+K</kbd> to search for a room and switch to it. However,
you may find that you keep searching for the same rooms again and again. There
is a faster way which does not require typing out the name of a room: setting
some custom keybindings for jumping to specific rooms.

For example, if you often talk in `#moment-client:matrix.org`, you may want to
set a custom binding <kbd>Ctrl+G Ctrl+M</kbd> for it. Right-click on it in the
room list and select "Copy room ID". Add the following entry to your
[config file](CONFIG.md):

``` python
class Keys:
    class Rooms:
        class Direct:
            "!wweZtVjpQSdWDDruas:mazie.rocks" = ["Ctrl+G,Ctrl+M"]
```
Where `!wweZtVjpQSdWDDruas:mazie.rocks` is the id for
`#moment-client:matrix.org` in this example.

Unlike using <kbd>Ctrl+K</kbd>, this approach is independent of room name
changes. For example, when another user changes their display name, the DM room
name would change, but not the ID, so the keybinding remains functional.

We suggest setting memorable keybindings. In the above example,
<kbd>Ctrl+G Ctrl+M</kbd> could mean "Go to Moment". Note that key chords in
Moment can be as long as you want - for example, you could have
<kbd>Ctrl+G Ctrl+M Ctrl+O</kbd> for `#moment-client:matrix.org` and
<kbd>Ctrl+G Ctrl+M Ctrl+I</kbd> for `#mirage-client:matrix.org`.
